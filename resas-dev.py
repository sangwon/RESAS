import json, requests

# some connection variables we'll need
baseURL = 'https://opendata.resas-portal.go.jp'
token = YOUR_KEY_HERE

# this is how we send the API key as a request header
requestHeaders = headers = {
    'X-API-KEY': token 
}

# set up the API endpoint we want to call first. this one just gets all the prefectures. 
endpoint = '/api/v1/prefectures'

# using the requests module, call the API
# note: this is sloppy, normally you need some error checking  
r = requests.get(baseURL + endpoint, headers=headers)

# display the response JSON on the console 
print(r.text)
print(r) # this displays the HTTP response code. 200 is good, 4XX indicates an error

